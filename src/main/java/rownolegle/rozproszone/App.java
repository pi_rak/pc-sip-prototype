package rownolegle.rozproszone;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import rownolegle.rozproszone.client.SipLayer;

import javax.sip.InvalidArgumentException;
import javax.sip.ObjectInUseException;
import javax.sip.PeerUnavailableException;
import javax.sip.TransportNotSupportedException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.TooManyListenersException;
import java.util.concurrent.ThreadLocalRandom;

public class App extends Application implements SipGuiController {

    private static final Logger logger = LogManager.getRootLogger();
    public static final Insets PADDING = new Insets(15, 15, 15, 15);
    public static final int DEFAULT_SPACING = 10;

    private Stage window;
    private BorderPane borderPane;
    private SipLayer layer;
    private TextArea mainChat;

    public static void main(String[] args) throws UnknownHostException, InvalidArgumentException, PeerUnavailableException, ParseException, TransportNotSupportedException, ObjectInUseException, TooManyListenersException {
        launch(args);
    }

    public void start(final Stage primaryStage) throws Exception {
        this.window = primaryStage;
        borderPane = new BorderPane();
        window.setTitle("SIP prototype");
        window.initStyle(StageStyle.UNDECORATED);

        final int RUNNING_PORT = ThreadLocalRandom.current().nextInt(1000, 9999);
        final String LOCAL_ADDRESS = InetAddress.getLocalHost().getHostAddress();
        layer = new SipLayer(this, LOCAL_ADDRESS, RUNNING_PORT);

        HBox topMenu = new HBox(DEFAULT_SPACING);
        HBox centerChat = new HBox(DEFAULT_SPACING);
        VBox bottomMenu = new VBox(DEFAULT_SPACING);

        Label recipientLabel = new Label("To: ");
        TextField recipientAddress = new TextField();
        topMenu.getChildren().addAll(recipientLabel, recipientAddress);
        topMenu.setAlignment(Pos.CENTER);
        topMenu.setPadding(PADDING);

        mainChat = new TextAreaTimestampDecorator();
        mainChat.setEditable(false);
        centerChat.getChildren().addAll(mainChat);
        centerChat.setPadding(PADDING);

        HBox sender = new HBox(DEFAULT_SPACING);
        TextField senderText = new TextField();
        Button sendButton = new Button("Send");
        sendButton.setOnAction(event -> layer.sendMessageOnExistingDialog(senderText.getText()));
        sender.setAlignment(Pos.CENTER);
        sender.setPadding(PADDING);
        HBox.setHgrow(senderText, Priority.ALWAYS);
        sender.getChildren().addAll(senderText, sendButton);
        HBox buttons = new HBox(DEFAULT_SPACING);
        Button inviteButton = new Button("INVITE");
        Button button2 = new Button("Button 2");
        Button button3 = new Button("Button 3");
        Button close = new Button("Close");
        inviteButton.setOnAction(event -> layer.sendInviteForDialog("sip:" + recipientAddress.getText(), "TEST MESSAGE WITH INVITE FOR DIALOG"));
        close.setOnAction(event -> close());
        buttons.getChildren().addAll(inviteButton, button2, button3, close);
        buttons.setAlignment(Pos.CENTER);
        buttons.setPadding(PADDING);
        bottomMenu.getChildren().addAll(sender, buttons);

        borderPane.setTop(topMenu);
        borderPane.setCenter(mainChat);
        borderPane.setBottom(bottomMenu);

        Scene scene = new Scene(borderPane, 500, 500);

        Delta delta = new Delta();
        scene.setOnMousePressed(event -> {
            delta.setX(window.getX() - event.getScreenX());
            delta.setY(window.getY() - event.getScreenY());
        });
        scene.setOnMouseDragged(event -> {
            window.setX(event.getScreenX() + delta.getX());
            window.setY(event.getScreenY() + delta.getY());
        });
        window.setOnCloseRequest(e -> close());
        window.setScene(scene);

        recipientAddress.setText(LOCAL_ADDRESS + ":" + RUNNING_PORT);
        mainChat.appendText("Welcome to SIP prototype. \n");
        mainChat.appendText("This layer is running on address: " + LOCAL_ADDRESS + " port: " + RUNNING_PORT + "\n");
        mainChat.appendText("Provide these in other layer to connect them. \n");
        window.show();
    }

    private void close() {
        // TODO: place code to run before close window
        window.close();
    }

    @Override
    public void displayOnChat(final String message) {
        mainChat.appendText(message + " \n");
    }

    private class Delta {
        private double x;
        private double y;

        double getX() {
            return x;
        }

        void setX(final double x) {
            this.x = x;
        }

        double getY() {
            return y;
        }

        void setY(final double y) {
            this.y = y;
        }
    }
}

