package rownolegle.rozproszone;

import javafx.scene.control.TextArea;

import java.text.SimpleDateFormat;
import java.util.Date;

class TextAreaTimestampDecorator extends TextArea {

    public TextAreaTimestampDecorator() {
    }

    @Override
    public void appendText(final String text) {
        String timestamp = new SimpleDateFormat("[yyyy:MM:dd:HH:mm:ss]").format(new Date());
        super.appendText(timestamp + text);
    }
}
