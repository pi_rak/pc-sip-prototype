package rownolegle.rozproszone.client;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import rownolegle.rozproszone.SipGuiController;

import javax.sip.*;
import javax.sip.address.Address;
import javax.sip.address.AddressFactory;
import javax.sip.header.*;
import javax.sip.message.MessageFactory;
import javax.sip.message.Request;
import javax.sip.message.Response;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.TooManyListenersException;

public class SipLayer implements SipListener {

    private final Logger logger = LogManager.getLogger(this.getClass().getSimpleName());

    private final Address contactAddress;
    private final ContactHeader contactHeader;

    private Properties properties;

    private SipStack sipStack;

    private SipFactory sipFactory;

    private AddressFactory addressFactory;

    private HeaderFactory headerFactory;

    private MessageFactory messageFactory;

    private SipProvider sipProvider;

    private Dialog dialog;

    private SipGuiController guiRef;

    public SipLayer(SipGuiController guiRef, String ip, int port) throws
            PeerUnavailableException, InvalidArgumentException, TransportNotSupportedException,
            TooManyListenersException, ObjectInUseException, ParseException {
        this.guiRef = guiRef;

        properties = new Properties();
        properties.setProperty("javax.sip.STACK_NAME", "SipClient");
        properties.setProperty("javax.sip.IP_ADDRESS", ip);

        sipFactory = SipFactory.getInstance();

        sipFactory.setPathName("gov.nist");
        sipStack = sipFactory.createSipStack(properties);
        headerFactory = sipFactory.createHeaderFactory();
        messageFactory = sipFactory.createMessageFactory();
        addressFactory = sipFactory.createAddressFactory();

        ListeningPoint udp = sipStack.createListeningPoint(ip, port, "udp");

        sipProvider = sipStack.createSipProvider(udp);
        sipProvider.addSipListener(this);

        this.contactAddress = this.addressFactory.createAddress("sip:" + getHost() + ":" + getPort());
        this.contactHeader = this.headerFactory.createContactHeader(contactAddress);
        logger.debug("SIP layer initialized. ");
    }

    public void processRequest(final RequestEvent requestEvent) {
        Request req = requestEvent.getRequest();

        String method = req.getMethod();
        if (!method.equals(Request.MESSAGE) && !method.equals(Request.REGISTER) && !method.equals(Request.INVITE) && !method.equals(Request.ACK)) {
            logger.error("bad request. ");
            return;
        }

        // TODO: look into this paralelly to processInvite() refactoring.
        if (method.equals(Request.INVITE)) {
            processInvite(requestEvent);
            return;
        }

        if (method.equals(Request.ACK)) {
            guiRef.displayOnChat("Incoming call. Connection established.");
            return;
        }

        FromHeader from = (FromHeader) req.getHeader("From");
        logger.info("Incoming message.");
        logger.info("From: " + from.getAddress().toString());
        logger.info("Message method is " + method);
        logger.info("Content constructed from raw content: \n\n" + new String(req.getRawContent()));
        guiRef.displayOnChat("[" + from.getAddress().toString() + "]: " + new String(req.getRawContent()));

        Response response = null;
        try { //Reply with OK
            response = messageFactory.createResponse(200, req);
            ToHeader toHeader = (ToHeader) response.getHeader(ToHeader.NAME);
            toHeader.setTag("4321"); //This is mandatory as per the spec.

            ServerTransaction st;
            if (requestEvent.getServerTransaction() == null) {
                st = sipProvider.getNewServerTransaction(req);
            } else {
                st = requestEvent.getServerTransaction();
            }
            response.addHeader(contactHeader);
            st.sendResponse(response);
            logger.info("Sent 200 response. ");
        } catch (Exception e) {
            logger.error("Cant send OK signal. Exception : ", e);
        }
    }

    // TODO: this method needs refactoring - be careful. demons inside.
    private void processInvite(final RequestEvent requestEvent) {
        SipProvider sipProvider = (SipProvider) requestEvent.getSource();
        Request request = requestEvent.getRequest();
        try {
            Response response = messageFactory.createResponse(Response.RINGING,
                    request);
            ServerTransaction st = requestEvent.getServerTransaction();

            if (st == null) {
                st = sipProvider.getNewServerTransaction(request);
            }
            dialog = st.getDialog();

            st.sendResponse(response);

            Response okResponse = messageFactory.createResponse(Response.OK,
                    request);

            Address address = this.addressFactory.createAddress("sip:" + getHost() + ":" + getPort());
            ContactHeader contactHeader = headerFactory
                    .createContactHeader(address);
            response.addHeader(contactHeader);
            ToHeader toHeader = (ToHeader) okResponse.getHeader(ToHeader.NAME);
            toHeader.setTag("4321"); // Application is supposed to set.
            okResponse.addHeader(contactHeader);

            ServerTransaction inviteTid = st;
            Request inviteRequest = request;

            Thread.sleep(1000);

            if (inviteTid.getState() != TransactionState.COMPLETED) {
                System.out.println("shootme: Dialog state before 200: "
                        + inviteTid.getDialog().getState());
                inviteTid.sendResponse(okResponse);
                System.out.println("shootme: Dialog state after 200: "
                        + inviteTid.getDialog().getState());
            }
        } catch (Exception e) {
            logger.error("Exception in new processInvite method: ", e);
        }
    }

    public void processResponse(final ResponseEvent responseEvent) {
        Response response = responseEvent.getResponse();
        int status = response.getStatusCode();

        logger.info("Received response: \n\n" + response.toString());

        if ((status >= 200) && (status < 300)) {
            logger.info("Message sent properly (received 200 after attempt to send message).");
            Dialog dialog = responseEvent.getClientTransaction().getDialog();
            if (dialog != null) {
                try {
                    Request request = dialog.createAck(((CSeqHeader) response.getHeader("CSeq")).getSeqNumber());
                    logger.debug("Dialog used to createAck. ");
                    guiRef.displayOnChat("Your recipient picked up the phone. ");
                    dialog.sendAck(request);
                    logger.debug("Sent ACK using dialog. ");
                    this.dialog = dialog;
                    logger.debug("Got non null dialog from responseEvent transaction.  Assigning it to this instance field. ");
                } catch (Exception e) {
                    logger.error("Could not send back ACK after receiving 200 when attempt to create dialog. Exception:", e);
                }
            }
            return;
        } else if (status == 180) {
            guiRef.displayOnChat("RINGING... ");
            return;
        }
        guiRef.displayOnChat("Error with sending your message. ");
        logger.error("Error sending message. ");
    }

    public void processTimeout(final TimeoutEvent timeoutEvent) {
        logger.error("time out error. ");
    }

    public void processIOException(final IOExceptionEvent exceptionEvent) {
        logger.error("IO exception error. ");
    }

    public void processTransactionTerminated(final TransactionTerminatedEvent transactionTerminatedEvent) {
        throw new UnsupportedOperationException();
    }

    public void processDialogTerminated(final DialogTerminatedEvent dialogTerminatedEvent) {
        throw new UnsupportedOperationException();
    }

    public String getHost() {
        return sipStack.getIPAddress();
    }

    public int getPort() {
        return sipProvider.getListeningPoints()[0].getPort();
    }

    public void sendRegisterStatefully(final String sipUri, String plainTextMessage) throws ParseException, InvalidArgumentException, SipException {
        Request request = createMessage(sipUri, plainTextMessage, Request.REGISTER);
        logger.debug("Created REGISTER request.");

        ClientTransaction transaction = this.sipProvider.getNewClientTransaction(request);
        logger.debug("Created transaction. ");
        transaction.sendRequest();
        logger.debug("Request sent through transaction. ");

    }

    public void sendRegisterStatelessly(final String sipUri, final String plainTextMessage) {
        try {
            Request request = createMessage(sipUri, plainTextMessage, Request.REGISTER);
            logger.debug("Created REGISTER request. ");

            this.sipProvider.sendRequest(request);
            logger.debug("Sent request statelessly. ");
        } catch (Exception e) {
            logger.error("Error while stateless registration. Exception: ", e);
        }
    }

    public void sendInviteForDialog(final String sipUri, final String plainTextMessage) {
        try {
            Request invite = createMessage(sipUri, plainTextMessage, Request.INVITE);
            logger.debug("Created INVITE request. ");

            ClientTransaction trans = sipProvider.getNewClientTransaction(invite);
            logger.debug("Created transaction. ");
            if (dialog == null) {
                dialog = trans.getDialog();
                logger.debug("Dialog variable was assigned. ");
                trans.sendRequest();
                logger.debug("INVITE Request sent in created transaction. ");
            } else {
                throw new IllegalStateException("Dialog already open on this instance. ");
            }
        } catch (Exception e) {
            logger.error("Error when sending the message inside dialog. Exception: ", e);
        }
    }

    public void sendByeForDialog() {
        try {
            Request request = this.dialog.createRequest(Request.BYE);
            logger.debug("Created BYE request using Dialog API. ");
            ClientTransaction transaction = this.sipProvider.getNewClientTransaction(request);
            logger.debug("Created transaction. ");
            this.dialog.sendRequest(transaction);
            logger.debug("Sent request through dialog. ");
        } catch (SipException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create message for given receiver, given content and type.
     *
     * @param sipUri           Where do you want to send the request to?
     * @param plainTextMessage Message to be inserted inside the request. You can access it with request.getContent() on the receiver side.
     * @param messageType      REGISTER, INVITE, BYE, etc. Check SIP protocol specification.
     * @return
     * @throws ParseException
     * @throws InvalidArgumentException
     */
    private Request createMessage(final String sipUri, String plainTextMessage, final String messageType) throws ParseException, InvalidArgumentException {
        // Get the destination address from the text field.
        Address addressTo = this.addressFactory.createAddress(sipUri);
        // Create the request URI for the SIP message.
        javax.sip.address.URI requestURI = addressTo.getURI();

        // Create the SIP message headers.

        // The "Via" headers.
        ArrayList viaHeaders = new ArrayList();
        ViaHeader viaHeader = this.headerFactory.createViaHeader(getHost(), getPort(), "udp", "z9hG4bKbranch1");
        viaHeaders.add(viaHeader);
        // The "Max-Forwards" header.
        MaxForwardsHeader maxForwardsHeader = this.headerFactory.createMaxForwardsHeader(70);
        // The "Call-Id" header.
        CallIdHeader callIdHeader = this.sipProvider.getNewCallId();
        // The "CSeq" header.
        CSeqHeader cSeqHeader = this.headerFactory.createCSeqHeader(1L, messageType);
        // The "From" header.
        FromHeader fromHeader = this.headerFactory.createFromHeader(this.contactAddress, "pi_rak");
        // The "To" header.
        ToHeader toHeader = this.headerFactory.createToHeader(addressTo, null);

        // Create the REGISTER request.
        Request request = this.messageFactory.createRequest(
                requestURI,
                messageType,
                callIdHeader,
                cSeqHeader,
                fromHeader,
                toHeader,
                viaHeaders,
                maxForwardsHeader);
        // Add the "Contact" header to the request.
        request.addHeader(contactHeader);

        ContentTypeHeader contentTypeHeader =
                headerFactory.createContentTypeHeader("text", "plain");
        request.setContent(plainTextMessage, contentTypeHeader);

        return request;
    }

    /**
     * Essentially, you're skipping the "create main elements" step when sending a message inside an existing dialog.
     * When you use an INVITE to create a dialog, don't forget to clean it up by sending an in-dialog BYE message at the end.
     * This technique is also used to refresh registrations and subscriptions.
     *
     * @param plainTextMessage
     */
    public void sendMessageOnExistingDialog(final String plainTextMessage) {
        if (dialog == null) {
            guiRef.displayOnChat("You have no open connections. ");
            logger.warn("Attempted to send message without existing dialog. ");
            return;
        }

        try {
            Request request = dialog.createRequest(Request.MESSAGE);

            request.setHeader(contactHeader);

            ContentTypeHeader contentTypeHeader =
                    headerFactory.createContentTypeHeader("text", "plain");
            request.setContent(plainTextMessage, contentTypeHeader);

            ClientTransaction trans = sipProvider.getNewClientTransaction(request);
            trans.sendRequest();
        } catch (Exception e) {
            logger.error("Error when sending the message inside existing dialog. Exception: ", e);
        }
    }

}
